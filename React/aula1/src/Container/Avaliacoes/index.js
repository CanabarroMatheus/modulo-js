import React from 'react';

import BotaoUi from '../../Components/BotaoUi';

const ListaAvaliacoes = props => {
  const { listaEpisodios } = props.location.state;

  return (
    <React.Fragment>
      <BotaoUi classe="preto" link="/" nome="Página Inicial" />
      {
        listaEpisodios.avaliados && (
          listaEpisodios.avaliados.map( ( e, i ) => {
            return <li key={ i }>{ `${ e.nome } - ${ e.nota }` }</li>;
          })
        )
      }
    </React.Fragment>
  );
} 

export default ListaAvaliacoes;