import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import App from './App';
import ListaAvaliacoes from './Avaliacoes';

export default class Rotas extends Component {
  render(){
    return (
      <Router>
        <Route path="/" exact component={ App } />
        <Route path="/avaliacoes" component={ ListaAvaliacoes } />
      </Router>
    );
  }
}

/* const PaginaTeste = () =>
  <div>
    <h1>Página Teste</h1>
    <Link to="/">Página App</Link>
  </div> */