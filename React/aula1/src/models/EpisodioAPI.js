import axios from 'axios';

const endereco = 'http://localhost:9000/';

export default class EpisodioAPI {
  buscar() {
    return axios.get( `${ endereco }episodios` ).then( e => e.data );
  }

  registrarNota( { nota, episodioId } ) {
    return axios.post( `${ endereco }notas`, { nota, episodioId } );
  }

  alterarNota( { id, nota } ) {
    return axios.patch( `${ endereco }notas/${ id }`, { nota } )
  }

  buscarNotasPorEpisodioId( episodioId ) {
    return axios.get( `${ endereco }notas?episodioId=${ episodioId }` ).then( e => {
      return e.data[0]
    } );
  }
}