import { Component } from "react";
import PropTypes from 'prop-types';

export default class Creditos extends Component {
  itemCreditos( valor ) {
    return <p>{ valor }</p>
  }

  render() {
    const { titulo, diretores, elenco } = this.props;

    return (
      <div className="creditos">
        <h1>{ titulo }</h1>
        <h2>Diretores</h2>
        { diretores.map( this.itemCreditos ) }
        <h2>Elenco</h2>
        { elenco.map( this.itemCreditos ) }
      </div>
    );
  }
}

Creditos.propTypes = {
  diretores: PropTypes.array.isRequired,
  elenco: PropTypes.array.isRequired
}
