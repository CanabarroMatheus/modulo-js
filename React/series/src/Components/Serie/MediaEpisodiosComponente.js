import { Component } from "react";
import PropTypes from 'prop-types';

export default class MediaEpisodios extends Component {
  render() {
    const { mediaEpisodios } = this.props;

    return(
      <div>
        <code>{ mediaEpisodios }</code>
      </div>
    );
  }
}

MediaEpisodios.propTypes = {
  mediaEpisodios: PropTypes.number.isRequired
}
