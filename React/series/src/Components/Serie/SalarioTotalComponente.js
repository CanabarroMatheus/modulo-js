import { Component } from "react";
import PropTypes from 'prop-types';

export default class SalarioTotal extends Component {
  render() {
    const { salarioTotal } = this.props;

    return (
      <div>
        <code>{ salarioTotal }</code>
      </div>
    );
  }
}

SalarioTotal.propTypes = {
  salarioTotal: PropTypes.string.isRequired
}
