import React, { Component } from "react";
import PropTypes from 'prop-types';

export default class SeriesInvalidas extends Component {
  render() {
    const { series } = this.props;

    return (
      <div>
        <code>{ JSON.stringify( series ) }</code>
      </div>
    );
  }
}

SeriesInvalidas.propTypes = {
  series: PropTypes.array.isRequired
}