import { Component } from "react";
import PropTypes from 'prop-types';

export default class SeriesPorAno extends Component {
  render() {
    const { series } = this.props;

    return (
      <div>
        <code>{ JSON.stringify( series, null, '\r' ) }</code>
      </div>
    );
  }
}

SeriesPorAno.propTypes = {
  series: PropTypes.array.isRequired
}