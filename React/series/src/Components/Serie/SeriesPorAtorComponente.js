import { Component } from "react";
import PropTypes from 'prop-types';

export default class SeriesPorAtor extends Component {
  render() {
    const { series } = this.props;

    return (
      <div>
        <code>{ series }</code>
      </div>
    );
  }
}

SeriesPorAtor.propTypes = {
  series: PropTypes.bool.isRequired
}
