import { Component } from "react";
import PropTypes from 'prop-types';

export default class SeriesPorGenero extends Component {
  render() {
    const { series } = this.props;

    return (
      <div>
        <code>{ JSON.stringify( series ) }</code>
      </div>
    );
  }
}

SeriesPorGenero.propTypes = {
  series: PropTypes.array.isRequired
}
