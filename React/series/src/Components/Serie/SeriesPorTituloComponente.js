import { Component } from "react";
import PropTypes from 'prop-types';

export default class SeriesPorTitulo extends Component {
  render() {
    const { series } = this.props;

    return (
      <div>
        <code>{ JSON.stringify( series ) }</code>
      </div>
    );
  }
}

SeriesPorTitulo.propTypes = {
  series: PropTypes.array.isRequired
}
