import React, { Component } from 'react';

import ListaSeries from '../Models/Serie/ListaSeries';

import './App.css';
import SeriesInvalidas from '../Components/Serie/SeriesInvalidasComponente';
import SeriesPorAno from '../Components/Serie/SeriesPorAnoComponente';
import SeriesPorAtor from '../Components/Serie/SeriesPorAtorComponente';
import MediaEpisodios from '../Components/Serie/MediaEpisodiosComponente';
import SalarioTotal from '../Components/Serie/SalarioTotalComponente';
import SeriesPorGenero from '../Components/Serie/SeriesPorGeneroComponente';
import SeriesPorTitulo from '../Components/Serie/SeriesPorTituloComponente';
import Creditos from '../Components/Serie/CreditosComponente';

export default class App extends Component {
  constructor( props ) {
    super( props );
    this.listaSeries = new ListaSeries();

    this.state = {
      series: this.listaSeries.series,
      invalidas: this.listaSeries.seriesInvalidas,
      porAno: this.listaSeries.filtrarPorAno( 2016 ),
      porAtor:this.listaSeries.procurarPorNome( 'Marcos' ),
      mediaEpisodios: this.listaSeries.mediaDeEpisodios,
      salarioTotal: this.listaSeries.totalSalarios( 0 ),
      buscarPorGenero: this.listaSeries.queroGenero( 'Acao' ),
      buscarPorTitulo: this.listaSeries.queroTitulo( 'The' ),
      hashtag: this.listaSeries.hashtagSecreta
    }
  }

  render() {
    const { series, invalidas, porAno, porAtor, mediaEpisodios, salarioTotal, buscarPorGenero, buscarPorTitulo, hashtag } = this.state;

    return (
      <div className="App">
        <SeriesInvalidas series={ invalidas } />

        {/* <div>
          <code>{ invalidas }</code>
        </div> */}

        <br/>

        <SeriesPorAno series={ porAno } />

        <br/>

        <SeriesPorAtor series={ porAtor } />

        <br/>

        <MediaEpisodios mediaEpisodios={ mediaEpisodios } />

        <br/>

        <SalarioTotal salarioTotal={ salarioTotal } />

        <br/>

        <SeriesPorGenero series={ buscarPorGenero } />

        <br/>

        <SeriesPorTitulo series={ buscarPorTitulo } />

        <br/>

        <Creditos titulo={ series[0].titulo } diretores={ series[0].organizarDiretoresPorUltimoNome() } elenco={ series[0].organizarElencoPorUltimoNome() } />

        <p>#{ hashtag }</p>
      </div>
    );
  }
}

// App.propTypes = {
//   series: PropTypes.string.isRequired,
//   invalidas: PropTypes.string.isRequired,
//   porAno: PropTypes.array.isRequired,
//   porAtor: PropTypes.bool.isRequired,
//   mediaEpisodios: PropTypes.number.isRequired,
//   salarioTotal: PropTypes.string.isRequired,
//   buscarPorGenero: PropTypes.array.isRequired,
//   buscarPorTitulo: PropTypes.string.isRequired
// }