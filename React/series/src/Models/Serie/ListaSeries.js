import SerieModel from "./Serie";

function verificarAnoEstreia( serie ) {
  return serie.anoEstreia > new Date().getFullYear();
}

function verificarUndefined( serie ) {
  const serieChaves = Object.keys(serie);
  let status = false;

  for (let i = 0; i < serieChaves.length; i++) {
    const itemSerie = serie[serieChaves[i]];

    if (itemSerie === null || itemSerie === undefined) {
      status = true;
    }
  }

  return status;
}

function verificarSerieInvalida( serie ) {
  if( verificarAnoEstreia( serie ) || verificarUndefined( serie ) ) {
    return true;
  }

  return false;
}

function verificarAbreviacao( nomes ) {
  let todosAbreviados = true;

  nomes.forEach((nome) => {
    if ( !nome.includes('.') ) {
      todosAbreviados = false;
    }
  })
  
  return todosAbreviados;
}

export default class ListaSeries {
  constructor() {
    this.series = [{
        titulo: "Stranger Things",
        anoEstreia: 2016,
        diretor: [
          "Matt Duffer",
          "Ross Duffer"
        ],
        genero: [
          "Suspense",
          "Ficcao Cientifica",
          "Drama"
        ],
        elenco: [
          "Winona Ryder",
          "David Harbour",
          "Finn Wolfhard",
          "Millie Bobby Brown",
          "Gaten Matarazzo",
          "Caleb McLaughlin",
          "Natalia Dyer",
          "Charlie Heaton",
          "Cara Buono",
          "Matthew Modine",
          "Noah Schnapp"
        ],
        temporadas: 2,
        numeroEpisodios: 17,
        distribuidora: "Netflix"
      },
      {
        titulo: "Game Of Thrones",
        anoEstreia: 2011,
        diretor: [
          "David Benioff",
          "D. B. Weiss",
          "Carolyn Strauss",
          "Frank Doelger",
          "Bernadette Caulfield",
          "George R. R. Martin"
        ],
        genero: [
          "Fantasia",
          "Drama"
        ],
        elenco: [
          "Peter Dinklage",
          "Nikolaj Coster-Waldau",
          "Lena Headey",
          "Emilia Clarke",
          "Kit Harington",
          "Aidan Gillen",
          "Iain Glen ",
          "Sophie Turner",
          "Maisie Williams",
          "Alfie Allen",
          "Isaac Hempstead Wright"
        ],
        temporadas: 7,
        numeroEpisodios: 67,
        distribuidora: "HBO"
      },
      {
        titulo: "The Walking Dead",
        anoEstreia: 2010,
        diretor: [
          "Jolly Dale",
          "Caleb Womble",
          "Paul Gadd",
          "Heather Bellson"
        ],
        genero: [
          "Terror",
          "Suspense",
          "Apocalipse Zumbi"
        ],
        elenco: [
          "Andrew Lincoln",
          "Jon Bernthal",
          "Sarah Wayne Callies",
          "Laurie Holden",
          "Jeffrey DeMunn",
          "Steven Yeun",
          "Chandler Riggs ",
          "Norman Reedus",
          "Lauren Cohan",
          "Danai Gurira",
          "Michael Rooker ",
          "David Morrissey"
        ],
        temporadas: 9,
        numeroEpisodios: 122,
        distribuidora: "AMC"
      },
      {
        titulo: "Band of Brothers",
        anoEstreia: 20001,
        diretor: [
          "Steven Spielberg",
          "Tom Hanks",
          "Preston Smith",
          "Erik Jendresen",
          "Stephen E. Ambrose"
        ],
        genero: [
          "Guerra"
        ],
        elenco: [
          "Damian Lewis",
          "Donnie Wahlberg",
          "Ron Livingston",
          "Matthew Settle",
          "Neal McDonough"
        ],
        temporadas: 1,
        numeroEpisodios: 10,
        distribuidora: "HBO"
      },
      {
        titulo: "The JS Mirror",
        anoEstreia: 2017,
        diretor: [
          "Lisandro",
          "Jaime",
          "Edgar"
        ],
        genero: [
          "Terror",
          "Caos",
          "JavaScript"
        ],
        elenco: [
          "Amanda de Carli",
          "Alex Baptista",
          "Gilberto Junior",
          "Gustavo Gallarreta",
          "Henrique Klein",
          "Isaias Fernandes",
          "João Vitor da Silva Silveira",
          "Arthur Mattos",
          "Mario Pereira",
          "Matheus Scheffer",
          "Tiago Almeida",
          "Tiago Falcon Lopes"
        ],
        temporadas: 5,
        numeroEpisodios: 40,
        distribuidora: "DBC"
      },
      {
        titulo: "Mr. Robot",
        anoEstreia: 2018,
        diretor: [
          "Sam Esmail"
        ],
        genero: [
          "Drama",
          "Techno Thriller",
          "Psychological Thriller"
        ],
        elenco: [
          "Rami Malek",
          "Carly Chaikin",
          "Portia Doubleday",
          "Martin Wallström",
          "Christian Slater"
        ],
        temporadas: 3,
        numeroEpisodios: 32,
        distribuidora: "USA Network"
      },
      {
        titulo: "Narcos",
        anoEstreia: 2015,
        diretor: [
          "Paul Eckstein",
          "Mariano Carranco",
          "Tim King",
          "Lorenzo O Brien"
        ],
        genero: [
          "Documentario",
          "Crime",
          "Drama"
        ],
        elenco: [
          "Wagner Moura",
          "Boyd Holbrook",
          "Pedro Pascal",
          "Joann Christie",
          "Mauricie Compte",
          "André Mattos",
          "Roberto Urbina",
          "Diego Cataño",
          "Jorge A. Jiménez",
          "Paulina Gaitán",
          "Paulina Garcia"
        ],
        temporadas: 3,
        numeroEpisodios: 30,
        distribuidora: null
      },
      {
        titulo: "Westworld",
        anoEstreia: 2016,
        diretor: [
          "Athena Wickham"
        ],
        genero: [
          "Ficcao Cientifica",
          "Drama",
          "Thriller",
          "Acao",
          "Aventura",
          "Faroeste"
        ],
        elenco: [
          "Anthony I. Hopkins",
          "Thandie N. Newton",
          "Jeffrey S. Wright",
          "James T. Marsden",
          "Ben I. Barnes",
          "Ingrid N. Bolso Berdal",
          "Clifton T. Collins Jr.",
          "Luke O. Hemsworth"
        ],
        temporadas: 2,
        numeroEpisodios: 20,
        distribuidora: "HBO"
      },
      {
        titulo: "Breaking Bad",
        anoEstreia: 2008,
        diretor: [
          "Vince Gilligan",
          "Michelle MacLaren",
          "Adam Bernstein",
          "Colin Bucksey",
          "Michael Slovis",
          "Peter Gould"
        ],
        genero: [
          "Acao",
          "Suspense",
          "Drama",
          "Crime",
          "Humor Negro"
        ],
        elenco: [
          "Bryan Cranston",
          "Anna Gunn",
          "Aaron Paul",
          "Dean Norris",
          "Betsy Brandt",
          "RJ Mitte"
        ],
        temporadas: 5,
        numeroEpisodios: 62,
        distribuidora: "AMC"
      }].map( serie => new SerieModel( serie ) );
  }

  get seriesInvalidas() {
    return this.series.filter(verificarSerieInvalida).map( serieInvalida => serieInvalida.titulo );
  }

  get mediaDeEpisodios() {
    let totalDeEpisodios = 0;

    this.series.forEach((serie) => {
      totalDeEpisodios += serie.numeroEpisodios;
    });

    return totalDeEpisodios / this.series.length;
  }

  get hashtagSecreta() {
    const serieTodaAbreviada = this.series.find( ({ elenco }) => verificarAbreviacao( elenco ) );

    let hashtag = '';

    console.log(serieTodaAbreviada);

    serieTodaAbreviada.elenco.forEach((nome) => {
      hashtag += nome.split(' ')[1].replace('.', '');
    });

    return hashtag;
  }

  filtrarPorAno( ano ) {
    return this.series.filter((serie) => {
      return serie.anoEstreia === ano;
    });
  }

  procurarPorNome( nome ) {
    let presenteEmElencos = false;

    this.series.forEach((serie) => {
      const elenco = serie.elenco;
      const encontrado = elenco.find(ator => ator === nome);

      presenteEmElencos = encontrado !== undefined ? true : false;
    });

    return presenteEmElencos;
  }

  totalSalarios( index ) {
    const serie = this.series[index];
    const numero = ( serie.diretor.length * 100000 ) + ( 40000 * serie.elenco.length );

    function arrendondar(numero, precisao = 2) {
      const fator = Math.pow(10, precisao);
      return Math.ceil(numero * fator) / fator; 
    }

    //metodo de tratamento do valor
    const quantidadeCasasMilhares = 3;
    let StringBuffer = [];

    let parteDecimal = arrendondar(Math.abs(numero) % 1);
    let parteInteira = Math.trunc(numero);
    let parteInteiraString = Math.abs(parteInteira).toString();
    let tamanhoParteInteira = parteInteiraString.length;

    let contador = 1;

    while (parteInteiraString.length > 0) {
      if (contador % quantidadeCasasMilhares === 0 && parteInteiraString.length > 3) {
        StringBuffer.push(`.${parteInteiraString.slice(tamanhoParteInteira - contador)}`);
        parteInteiraString = parteInteiraString.slice(0, tamanhoParteInteira - contador);
      } else if (parteInteiraString.length <= quantidadeCasasMilhares) {
        StringBuffer.push(parteInteiraString);
        parteInteiraString = "";
      }
      contador++;
    }
    StringBuffer.push(parteInteiraString);

    let decimalNaString = parteDecimal.toString().replace("0.", "").padStart(2, 0);
    const numeroFormatado = `R$ ${StringBuffer.reverse().join("")},${decimalNaString}`;

    return numeroFormatado;

    // return `R$ ${ ( serie.diretor.length * 100000 ) + ( 40000 * serie.elenco.length ) }`;
  }

  queroGenero( genero ) {
    return this.series.filter((serie) => {
      return serie.genero.find( generoSerie => generoSerie === genero ) !== undefined;
    }).map( serie => serie.titulo );
  }

  queroTitulo( titulo ) {
    return this.series.filter( serie => serie.titulo.includes( titulo ) ).map( serie => serie.titulo );
  }
}