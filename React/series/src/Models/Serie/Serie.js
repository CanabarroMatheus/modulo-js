export default class SerieModel {
  constructor( { titulo, anoEstreia, diretor, genero, elenco, temporadas, numeroEpisodios, distribuidora } ) {
    this.titulo = titulo;
    this.anoEstreia = anoEstreia;
    this.diretor = diretor;
    this.genero = genero;
    this.elenco = elenco;
    this.temporadas = temporadas;
    this.numeroEpisodios = numeroEpisodios;
    this.distribuidora = distribuidora;
  }

  organizarDiretoresPorUltimoNome() {
    const diretores = this.diretor.sort( ( nome1, nome2 ) => {
      if( nome1.split(' ')[1] < nome2.split(' ')[1] ) {
        return -1;
      } else if ( nome1.split(' ')[1] > nome2.split(' ')[1] ) {
        return 1;
      } else if ( nome1.split(' ')[0] < nome2.split(' ')[0] ) {
        return -1;
      } else if ( nome1.split(' ')[0] > nome2.split(' ')[0] ) {
        return 1;
      } else {
        return 0;
      }
    });

    return diretores;
  }

  organizarElencoPorUltimoNome() {
    const elenco = this.elenco.sort( ( nome1, nome2 ) => {
      if( nome1.split(' ')[1] < nome2.split(' ')[1] ) {
        return -1;
      } else if ( nome1.split(' ')[1] > nome2.split(' ')[1] ) {
        return 1;
      } else if ( nome1.split(' ')[0] < nome2.split(' ')[0] ) {
        return -1;
      } else if ( nome1.split(' ')[0] > nome2.split(' ')[0] ) {
        return 1;
      } else {
        return 0;
      }
    });

    return elenco;
  }
}