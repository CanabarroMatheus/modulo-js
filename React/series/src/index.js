import React from 'react';
import ReactDOM from 'react-dom';
import App from './Container/App.js';

ReactDOM.render( <App />, document.getElementById('root') );
