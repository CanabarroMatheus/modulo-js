console.log("Vindo do arquivo");

/*
 * Não se usa mais 'var'!!!
 */
// var nome = "Matheus";

let nome2 = "Matheus";
const nome3 = "Matheus";

let nome1 = "Math";

  {
    let nome1 = "Matheuz";
    nome1 = "Suehtam";
    console.log(nome1);
  }

console.log(nome1);

/**
 * CONST
 * 
 * Constantes são de fato constantes, MAS objetos do tipo CONST podem ter seus atributos alterados
 */

// const pessoa = {
//   nome: "Matheus",
//   idade: 20
// }

//Esse método faz com que nem mesmo os atributo do Objeto possam ser reescritos
// Object.freeze(pessoa);

// pessoa.nome = "Math";
// console.log(pessoa.nome);

/**
 * ESPETACULAR
*/

let soma = 1 + 2;
let soma1 = "1" + 2 + 3;
let soma2 = 1 + "2" + 3;
let soma3 = 1 + 2 + "3";

console.log(soma);
console.log(soma1);
console.log(soma2);
console.log(soma3);

/**
 * Funções
*/

let nome = "Marcão";
let idade = 31;
let semestre = 5;
let notas = [10.0, 3, 9, 9]

function criarAluno(nome, idade, semestre, notas = []) {

  function aprovadoOuReprovado(notas) {
    if (notas.length == 0) {
      return "Sem notas";
    }

    let somatoria = 0;

    for (let i = 0; i < notas.length; i++) {
      somatoria += notas[i];
    }

    return (somatoria / notas.length) > 7.0 ? "Aprovado" : "Reprovado";
  }
  
  const aluno = {
    nome: nome,
    idade: idade,
    semestre: semestre,
    notas: notas,
    status: aprovadoOuReprovado(notas)
  }

  console.log(aluno);

  return aluno;
}

criarAluno(nome, idade, semestre);

let alunoExterno = criarAluno(nome, idade, semestre, notas);

console.log(alunoExterno);

// alunoExterno.nota = 11;

// console.log(alunoExterno);