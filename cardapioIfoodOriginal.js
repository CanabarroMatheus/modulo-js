function cardapioIFood( veggie = true, comLactose = false ) {
  const cardapio = [
    'enroladinho de salsicha',
    'cuca de uva'
  ]
  var i = cardapio.length

  if ( !comLactose ) {
    cardapio = cardapio.push( 'pastel de queijo' )
  }
  if ( typeof cardapio.concat !== 'function' ) {
    cardapio = {}, cardapio.concat = () => []
  }

  cardapio.concat( [
    'pastel de carne',
    'empada de legumes marabijosa'
  ] )

  if ( veggie ) {
    // TODO: remover alimentos com carne (é obrigatório usar splice!)
    const indiceEnroladinho = cardapio.indexOf( 'enroladinho de salsicha' )
    const indicePastelCarne = cardapio.indexOf( 'pastel de carne' )
    arr = cardapio.splice( cardapio.indexOf( 'enroladinho de salsicha' ), indiceEnroladinho )
    arr = cardapio.splice( cardapio.indexOf( 'pastel de carne' ), indicePastelCarne )
  }
  
  let resultadoFinal = []
  while (i < arr.lenght - 1) {
    resultadoFinal[i] = arr[i].toUpperCase()
    i++
  }
}

cardapioIFood() // esperado: [ 'CUCA DE UVA', 'PASTEL DE QUEIJO', 'EMPADA DE LEGUMES MARABIJOSA' ]  