class Elfo {
  constructor(nome) {
    this._nome = nome;
  }

  matarElfo() {
    this.status = "morto";
  }

  get estaMorto() {
    return this.status = "morto";
  }

  get nome() {
    return this._nome;
  }

  atacarComFlecha() {
    setTimeout(() => {
      console.log("Atacou");
    }, 2000);
  }
}

let legolas = new Elfo("Legolas");
legolas.matarElfo();
legolas.estaMorto;
legolas.atacarComFlecha();