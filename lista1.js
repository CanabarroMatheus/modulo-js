/** 
 * Exercício 1
 */

function calcularCirculo(circulo) {
  const circuloFuncao = circulo;

  if(circuloFuncao.tipoCalculo == "A") {
    return Math.ceil(Math.PI * Math.pow(circuloFuncao.raio, 2));
  } else if(circuloFuncao.tipoCalculo == "C") {
    return Math.PI * (2 * circuloFuncao.raio);
  } else {
    return "Tipo de calculo inválido!"
  }
}

const circulo1 = {
  raio: 3,
  tipoCalculo: "A"
}

const circulo2 = {
  raio: 3,
  tipoCalculo: "C"
}

console.log(calcularCirculo(circulo1));
console.log(calcularCirculo(circulo2));

/** 
 * Exercício 2
 */

function isBissexto(ano) {
  function divisivelPor100E400(anoDivisor) {
    return ano % 100 == 0 && ano % 400 == 0;
  }

  if (ano % 4 == 0 || divisivelPor100E400(ano)) {
    return "É bissexto!";
  } else {
    return "Não é bissexto!"
  }
}

console.log("O ano de " + 2016 + " " + isBissexto(2016));
console.log("O ano de " + 2000 + " " + isBissexto(2000));
console.log("O ano de " + 2002 + " " + isBissexto(2002));

// function bissexto(ano) {
//   return (ano % 400 == 0) || (ano % 4 == 0 && ano % 100 != 0);
// }

// console.log("O ano de " + 2016 + bissexto(2016) == true ? " é bissexto!" : " não é bissexto...");
// console.log("O ano de " + 2000 + bissexto(2000) == true ? " é bissexto!" : " não é bissexto...");
// console.log("O ano de " + 2017 + bissexto(2017) == true ? " é bissexto!" : " não é bissexto...");

/** 
 * Exercício 3
 */

function somarPares(sequencia) {
  let soma = 0;

  for (let i = 0; i < sequencia.length; i++) {
    if (i % 2 == 0) {
      soma += sequencia[i];
    }
  }

  return soma;
}

console.log(somarPares([1, 56, 4.34, 6, -2]));

/** 
 * Exercício 4
 */

// function adicionar(numero1) {
//   function adicionar2(numero2) {
//     return numero1 + numero2;
//   }

//   return adicionar2;
// }

let adicionar = numero1 => numero2 => numero1 + numero2;

console.log(adicionar(2)(2));

/** 
 * Exercício 5
 */

// function imprimirBRL(valor) {
//   function converterParaBRL(decimal) {
//     return decimal.toLocaleString("pt-BR", {style: "currency", currency: "BRL"});
//   }

//   function arrendondar(decimal) {
//     let decimalCom2casas = decimal.toFixed(2);
//     if(parseFloat(decimalCom2casas) < valor) {
//       decimal += 0.01;
//     }

//     return parseFloat(decimal.toFixed(2));
//   }

//   return converterParaBRL(arrendondar(valor));
// }

// console.log(imprimirBRL(2.691));

const moedas = (function() {
  //privado
  function imprimirMoeda(parametro) {
    function arrendondar(numero, precisao = 2) {
      const fator = Math.pow(10, precisao);
      return Math.ceil(numero * fator) / fator; 
    }

    const {
      numero,
      separadorMilhar,
      separadorDecimal,
      colocarMoeda,
      colocarNegativo
    } = parametro;

    //metodo de tratamento do valor
    const quantidadeCasasMilhares = 3;
    let StringBuffer = [];

    let parteDecimal = arrendondar(Math.abs(numero) % 1);
    let parteInteira = Math.trunc(numero);
    let parteInteiraString = Math.abs(parteInteira).toString();
    let tamanhoParteInteira = parteInteiraString.length;

    let contador = 1;

    while (parteInteiraString.length > 0) {
      if (contador % quantidadeCasasMilhares == 0 && parteInteiraString.length > 3) {
        StringBuffer.push(`${separadorMilhar}${parteInteiraString.slice(tamanhoParteInteira - contador)}`);
        parteInteiraString = parteInteiraString.slice(0, tamanhoParteInteira - contador);
      } else if (parteInteiraString.length <= quantidadeCasasMilhares) {
        StringBuffer.push(parteInteiraString);
        parteInteiraString = "";
      }
      contador++;
    }
    StringBuffer.push(parteInteiraString);

    let decimalNaString = parteDecimal.toString().replace("0.", "").padStart(2, 0);
    const numeroFormatado = `${StringBuffer.reverse().join("")}${separadorDecimal}${decimalNaString}`;

    return parteInteira >= 0 ? colocarMoeda(numeroFormatado) : colocarNegativo(numeroFormatado);
  };

  //público
  return {
    imprimirBRL: (numero) => 
      imprimirMoeda({
        numero,
        separadorDecimal: ",",
        separadorMilhar: ".",
        colocarMoeda: numeroFormatado => `R$ ${numeroFormatado}`,
        colocarNegativo:  numeroFormatado => `-R$ ${numeroFormatado}`
      }),
    imprimirUSD: (numero) => 
      imprimirMoeda({
        numero,
        separadorDecimal: ",",
        separadorMilhar: ".",
        colocarMoeda: numeroFormatado => `$ ${numeroFormatado}`,
        colocarNegativo:  numeroFormatado => `-$ ${numeroFormatado}`
      })
  };
})();

console.log(moedas.imprimirBRL(2313477.0135));

/**
 * Exercício extra
 */

const divisao = divisor => numero => (numero/divisor);
const divisivelPor = divisao(2);

console.log(divisivelPor(4));
console.log(divisivelPor(6));
console.log(divisivelPor(8));
console.log(divisivelPor(10));