function cardapioIFood(veggie = true, comLactose = true) {
  const cardapio = [
    'enroladinho de salsicha',
    'cuca de uva',
    'empada de legumes marabijosa',
    'pastel de carne',
    'pastel de queijo' 
  ]

  if(!comLactose) {
    const indicePastelQueijo = cardapio.indexOf('pastel de queijo');
    cardapio.splice(indicePastelQueijo, 1);
  }

  if(veggie) {
    const indicePastelCarne = cardapio.indexOf('pastel de carne');
    cardapio.splice(indicePastelCarne, 1);
    const indiceEnroladinho = cardapio.indexOf('enroladinho de salsicha');
    cardapio.splice(indiceEnroladinho, 1);
  }

  cardapio.forEach(element => {
    element = element.toUpperCase();
  });

  return cardapio;
}

console.log(cardapioIFood()); // esperado: [ 'CUCA DE UVA', 'PASTEL DE QUEIJO', 'EMPADA DE LEGUMES MARABIJOSA' ]