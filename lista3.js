class TimeEsport {
  constructor(nome, tipoEsport, liga) {
    this.nome = nome;
    this.tipoEsport = tipoEsport;
    this.status = "ativo";
    this.liga = liga;
    this.jogadores = []
  }

  adicionarJogador(jogador) {
    if (typeof jogador == 'object') {
      this.jogadores = [...this.jogadores, jogador];
    }
  }

  encontrarJogador(valor) {
    return this.jogadores.find(jogador => jogador.nome == valor || jogador.camisa == valor);
  }
}

class Jogador {
  constructor(nome, camisa) {
    this.nome = nome;
    this.camisa = camisa;
  }
}

let paiN = new TimeEsport("PaiN Gaming", "League of Legends", "CBLOL");
paiN.adicionarJogador(new Jogador("Tinowns", 6));
paiN.adicionarJogador(new Jogador("BrTT", 10));
paiN.adicionarJogador(new Jogador("Robo", 1));
paiN.adicionarJogador(new Jogador("Cariok", 2));
paiN.adicionarJogador(new Jogador("Luci", 9));
console.log(paiN);
console.log(paiN.jogadores);
console.log(paiN.encontrarJogador("Robo"));
console.log(paiN.encontrarJogador(10));