const pokeApi = new PokeApi();
const cookies = new Cookies(); // eslint-disable-line no-undef

window.onload = () => {
  mostrarLoading(); // eslint-disable-line no-undef
  if ( cookies.verificarQuantidade() > 0 ) {
    const poke = new Pokemon( cookies.obter( cookies.ultimoPokemon ) );
    document.getElementById( 'pokemon-search' ).value = cookies.ultimoPokemon;

    fecharLoading(); // eslint-disable-line no-undef
    organizarCard( 0 ); // eslint-disable-line no-undef
    renderizar( poke ); // eslint-disable-line no-undef
    habilitarBotoes(); // eslint-disable-line no-undef
  } else {
    fecharLoading(); // eslint-disable-line no-undef
  }
}

function buscarPokemon() { // eslint-disable-line no-unused-vars
  mostrarLoading(); // eslint-disable-line no-undef
  esconderErro(); // eslint-disable-line no-undef
  const id = document.getElementById( 'pokemon-search' ).value;

  setTimeout( () => {
    if ( id > 0 ) {
      if ( id.length > 0 && id !== 0) {
        if ( cookies.ultimoPokemon !== id ) {
          if ( cookies.obter( id ) == null ) {
            pokeApi.buscarEspecifico( id )
              .then( pokemon => {
                cookies.adicionar( pokemon );
                const poke = new Pokemon( pokemon );
                fecharLoading(); // eslint-disable-line no-undef
                organizarCard( 0 ); // eslint-disable-line no-undef
                limparListas(); // eslint-disable-line no-undef
                renderizar( poke ); // eslint-disable-line no-undef
                habilitarBotoes(); // eslint-disable-line no-undef
              } )
              .catch( () => {
                fecharLoading(); // eslint-disable-line no-undef
                organizarCard( 5 ); // eslint-disable-line no-undef
                limparGeral(); // eslint-disable-line no-undef
                limparListas(); // eslint-disable-line no-undef
                exibirErro(); // eslint-disable-line no-undef
                desabilitarBotoes(); // eslint-disable-line no-undef
              } );
          } else {
            const poke = new Pokemon( cookies.obter( id ) );
            fecharLoading(); // eslint-disable-line no-undef
            organizarCard( 0 ); // eslint-disable-line no-undef
            renderizar( poke ); // eslint-disable-line no-undef
            habilitarBotoes(); // eslint-disable-line no-undef
          }
        }
      } else {
        fecharLoading(); // eslint-disable-line no-undef
        organizarCard( 5 ); // eslint-disable-line no-undef
        limparGeral(); // eslint-disable-line no-undef
        limparListas(); // eslint-disable-line no-undef
        exibirErro(); // eslint-disable-line no-undef
        desabilitarBotoes(); // eslint-disable-line no-undef
      }
    } else {
      fecharLoading(); // eslint-disable-line no-undef
      organizarCard( 5 ); // eslint-disable-line no-undef
      limparGeral(); // eslint-disable-line no-undef
      limparListas(); // eslint-disable-line no-undef
      exibirErro(); // eslint-disable-line no-undef
      desabilitarBotoes(); // eslint-disable-line no-undef
    }
  }, 2000 );
}

renderizar = ( pokemon ) => { // eslint-disable-line no-undef
  const nomePokemon = document.getElementById( 'pokemon-nome' );
  const imagemPokemon = document.getElementById( 'pokemon-imagem' );
  const dadosPokemon = document.getElementById( 'pokemon-informacao' );

  nomePokemon.innerHTML = pokemon.nome.toUpperCase();
  imagemPokemon.src = pokemon.imagens.front_default;

  const infoNome = dadosPokemon.querySelector( '#pokemon-info-nome' );
  infoNome.innerHTML = pokemon.nome;

  const infoAltura = dadosPokemon.querySelector( '#pokemon-info-altura' );
  infoAltura.innerHTML = pokemon.altura;

  const infoPeso = dadosPokemon.querySelector( '#pokemon-info-peso' );
  infoPeso.innerHTML = pokemon.peso;

  const infoTipoLista = dadosPokemon.querySelector( '#pokemon-info-tipos' );

  for ( let i = 0; i < pokemon.tipos.length; i += 1 ) {
    const tipo = pokemon.tipos[i];
    const li = document.createElement( 'li' );
    li.innerHTML = capitalizarPrimeiraLetra( tipo.type.name ); // eslint-disable-line no-undef
    infoTipoLista.appendChild( li );
  }

  const infoHabilidadeLista = dadosPokemon.querySelector( '#pokemon-info-habilidades' );

  for ( let i = 0; i < pokemon.habilidades.length; i += 1 ) {
    const habilidade = pokemon.habilidades[i];
    const li = document.createElement( 'li' );
    // eslint-disable-next-line no-undef
    li.innerHTML = capitalizarPrimeiraLetra( habilidade.ability.name );
    infoHabilidadeLista.appendChild( li );
  }

  const infoMovimentoLista = dadosPokemon.querySelector( '#pokemon-info-movimentos' );

  for ( let i = 0; i < pokemon.movimentos.length; i += 1 ) {
    const movimento = pokemon.movimentos[i];
    const li = document.createElement( 'li' );
    li.innerHTML = capitalizarPrimeiraLetra( movimento.move.name ); // eslint-disable-line no-undef
    infoMovimentoLista.appendChild( li );
  }

  const infoStatsLista = dadosPokemon.querySelector( '#pokemon-info-stats' );

  for ( let i = 0; i < pokemon.estatisticas.length; i += 1 ) {
    const estatistica = pokemon.estatisticas[i];
    const tr = document.createElement( 'tr' );
    const tdNome = document.createElement( 'td' );
    const tdValor = document.createElement( 'td' );
    const tdEsforco = document.createElement( 'td' );
    // eslint-disable-next-line no-undef
    tdNome.innerHTML = capitalizarPrimeiraLetra( estatistica.stat.name );
    tdValor.innerHTML = estatistica.base_stat;
    tdEsforco.innerHTML = estatistica.effort;

    tr.appendChild( tdNome );
    tr.appendChild( tdValor );
    tr.appendChild( tdEsforco );

    infoStatsLista.appendChild( tr );
  }
}

function buscarPokemonAleatorio() { // eslint-disable-line no-unused-vars
  mostrarLoading(); // eslint-disable-line no-undef
  const id = gerarNumeroAleatorio(); // eslint-disable-line no-undef

  setTimeout( () => {
    if ( cookies.obter( id ) == null ) {
      limparListas(); // eslint-disable-line no-undef
      document.getElementById( 'pokemon-search' ).value = id;

      pokeApi.buscarEspecifico( id )
        .then( pokemon => {
          cookies.adicionar( pokemon )
          const poke = new Pokemon( pokemon );
          fecharLoading(); // eslint-disable-line no-undef
          organizarCard( 0 ); // eslint-disable-line no-undef
          renderizar( poke ); // eslint-disable-line no-undef
          habilitarBotoes(); // eslint-disable-line no-undef
        } )
        .catch( () => {
          fecharLoading(); // eslint-disable-line no-undef
          organizarCard( 5 ); // eslint-disable-line no-undef
          limparGeral(); // eslint-disable-line no-undef
          exibirErro(); // eslint-disable-line no-undef
          desabilitarBotoes(); // eslint-disable-line no-undef
        } );
    } else {
      const poke = new Pokemon( cookies.obter( id ) );

      fecharLoading(); // eslint-disable-line no-undef
      organizarCard( 0 ); // eslint-disable-line no-undef
      renderizar( poke ); // eslint-disable-line no-undef
      habilitarBotoes(); // eslint-disable-line no-undef
    }
  }, 2000 );
}

limparGeral = () => { // eslint-disable-line no-undef
  const dadosPokemon = document.getElementById( 'pokemon-informacao' );
  const nomePokemon = document.getElementById( 'pokemon-nome' );
  const imagemPokemon = document.getElementById( 'pokemon-imagem' );

  const infoNome = dadosPokemon.querySelector( '#pokemon-info-nome' );
  const infoAltura = dadosPokemon.querySelector( '#pokemon-info-altura' );
  const infoPeso = dadosPokemon.querySelector( '#pokemon-info-peso' );

  nomePokemon.innerHTML = '';
  imagemPokemon.src = '';

  infoNome.innerHTML = '';
  infoAltura.innerHTML = '';
  infoPeso.innerHTML = '';
}

limparListas = () => { // eslint-disable-line no-undef
  const dadosPokemon = document.getElementById( 'pokemon-informacao' );
  const infoTipoLista = dadosPokemon.querySelector( '#pokemon-info-tipos' );
  const infoHabilidadeLista = dadosPokemon.querySelector( '#pokemon-info-habilidades' );
  const infoMovimentoLista = dadosPokemon.querySelector( '#pokemon-info-movimentos' );
  const infoStatsLista = dadosPokemon.querySelector( '#pokemon-info-stats' );

  const listas = [infoTipoLista, infoHabilidadeLista, infoMovimentoLista, infoStatsLista];

  for ( let i = 0; i < listas.length; i += 1 ) {
    limparElemento( listas[i] ); // eslint-disable-line no-undef
  }
}

esconderErro = () => { // eslint-disable-line no-undef
  const card = document.querySelector( '.info-error' );
  card.hidden = true;
}

exibirErro = () => { // eslint-disable-line no-undef
  const card = document.querySelector( '.info-error' );
  card.hidden = false;
}

habilitarBotoes = () => { // eslint-disable-line no-undef
  const botoes = document.querySelector( '.button-group' ).children;

  for ( let i = 0; i < botoes.length; i += 1 ) {
    const botao = botoes[i];
    botao.disabled = false;
  }
}

desabilitarBotoes = () => { // eslint-disable-line no-undef
  const botoes = document.querySelector( '.button-group' ).children;

  for ( let i = 0; i < botoes.length; i += 1 ) {
    const botao = botoes[i];
    botao.disabled = true;
  }
}

organizarCard = ( valor ) => { // eslint-disable-line no-undef
  const dadosPokemon = document.getElementById( 'pokemon-informacao' );

  const infoGeral = dadosPokemon.querySelector( '.info-geral' );
  const infoHabilidades = dadosPokemon.querySelector( '.info-habilidades' );
  const infoMovimentos = dadosPokemon.querySelector( '.info-movimentos' );
  const infoStats = dadosPokemon.querySelector( '.info-stats' );

  switch ( valor ) {
    case 0:
      infoGeral.hidden = false;
      infoHabilidades.hidden = true;
      infoMovimentos.hidden = true;
      infoStats.hidden = true;

      break;

    case 1:
      infoGeral.hidden = true;
      infoHabilidades.hidden = false;
      infoMovimentos.hidden = true;
      infoStats.hidden = true;

      break;

    case 2:
      infoGeral.hidden = true;
      infoHabilidades.hidden = true;
      infoMovimentos.hidden = false;
      infoStats.hidden = true;

      break;

    case 3:
      infoGeral.hidden = true;
      infoHabilidades.hidden = true;
      infoMovimentos.hidden = true;
      infoStats.hidden = false;

      break;

    default:
      infoGeral.hidden = true;
      infoHabilidades.hidden = true;
      infoMovimentos.hidden = true;
      infoStats.hidden = true;

      break;
  }
}
