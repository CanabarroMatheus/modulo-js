function capitalizarPrimeiraLetra( frase ) { // eslint-disable-line no-unused-vars
  return frase.charAt( 0 ).toUpperCase() + frase.slice( 1 );
}

function limparElemento( elemento ) { // eslint-disable-line no-unused-vars
  const editElemento = elemento;
  editElemento.innerHTML = '';
}

function gerarNumeroAleatorio() { // eslint-disable-line no-unused-vars
  return parseInt( Math.random() * ( 894 - 1 ) + 1, 10 );
}

function mostrarLoading() { // eslint-disable-line no-unused-vars
  document.getElementById( 'button-sorte' ).disabled = true;
  document.getElementById( 'loader' ).style.display = 'block';
}

function fecharLoading() { // eslint-disable-line no-unused-vars
  document.getElementById( 'button-sorte' ).disabled = false;
  document.getElementById( 'loader' ).style.display = 'none';
}
