let pokeApi = new PokeApi();
pokeApi.buscarEspecifico(112)
  .then(pokemon => {
    let poke = new Pokemon(pokemon);
    renderizar(poke);
  });

renderizar = (pokemon) => {
  let dadosPokemon = document.getElementById("dadosPokemon");

  let nome = dadosPokemon.querySelector(".nome");
  nome.innerHTML = pokemon.nome;

  let imagem = dadosPokemon.querySelector(".thumb");
  imagem.src = pokemon.imagem;

  let altura = dadosPokemon.querySelector(".altura");
  altura.innerHTML = pokemon.altura;
}