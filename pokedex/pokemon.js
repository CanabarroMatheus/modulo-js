class Pokemon {
  constructor(pokemonData) {
    this._nome = pokemonData.name;
    this._imagem = pokemonData.sprites.front_default;
    this._altura = pokemonData.height;
  }

  get nome() {
    return this._nome;
  }

  get imagem() {
    return this._imagem;
  }

  get altura() {
    return `${this._altura * 10} cm`;
  }
}